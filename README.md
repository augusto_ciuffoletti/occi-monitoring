#Summary#

This is the code for the demo of the OCCI Monitoring-as-a-Service extension. It can be imported as an Eclipse project using Egit.

To see it in action please download the Vagrant provisioning at  

https://bitbucket.org/augusto_ciuffoletti/occimon-demo/